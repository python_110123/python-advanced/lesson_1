# Створіть список цілих чисел. Отримайте список квадратів непарних
# чисел із цього списку.
def squares_of_even(lst):
    """Генератор, який повертає список квадратів непарних чисел"""
    return (x**2 for x in lst if x % 2 != 0)
# Альтернативний варіант: [x**2 for x in filter(lambda x: x % 2 != 0, my_list)]


numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
squares = squares_of_even(numbers)
print(list(squares))
