# За допомогою написаного Вами декоратору заміряйте та порівняйте
# швидкість роботи цих 4х варіантів.
import time
from task_3 import (
    fibonacci,
    fibonacci_with_cache,
    fibonacci_with_limited_cache,
    fibonacci_with_limited_cache_16,
)

if __name__ == "__main__":
    print("Перші 25 чисел Фібоначі без кешування:")
    print(fibonacci(25))
    start_time = time.perf_counter()
    f1 = fibonacci(25)
    end_time = time.perf_counter()
    run_time = end_time - start_time
    print(f"Час виконання: {run_time:.8f} секунд")

    print("Перші 25 чисел Фібоначі з кешем довільної довжини:")
    print(fibonacci_with_cache(25))
    start_time = time.perf_counter()
    f2 = fibonacci_with_cache(25)
    end_time = time.perf_counter()
    run_time = end_time - start_time
    print(f"Час виконання: {run_time:.8f} секунд")

    print("Перші 25 чисел Фібоначі з кешем довжиною 10 елементів:")
    print(fibonacci_with_limited_cache(25))
    start_time = time.perf_counter()
    f3 = fibonacci_with_limited_cache(25)
    end_time = time.perf_counter()
    run_time = end_time - start_time
    print(f"Час виконання: {run_time:.8f} секунд")

    print("Перші 25 чисел Фібоначі з кешем довжиною 16 елементів:")
    print(fibonacci_with_limited_cache_16(25))
    start_time = time.perf_counter()
    f4 = fibonacci_with_limited_cache_16(25)
    end_time = time.perf_counter()
    run_time = end_time - start_time
    print(f"Час виконання: {run_time:.9f} секунд")

# ПЕРЕВІРКА ДЗ на правки: В завданні 4 потрібно використати власний декоратор
#@timer_decorator
#def fibonacci_lru(index):
    #@functools.lru_cache(maxsize=10)
    #def fibonacci_1(ind):
        #if ind < 2:
            #return ind
        #return fibonacci_1(ind - 1) + fibonacci_1(ind - 2)

    #result_list = [fibonacci_1(i) for i in range(index + 1)]
    #return result_list
