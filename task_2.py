# Напишіть декоратор, який буде заміряти час виконання для наданої функції.
import functools
import time


def timer(func):
    """Надрукує час виконання оформленої функції"""

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Витрачений час на {func.__name__!r} функцію: {run_time:.6f} секунд")
        return result

    return wrapper


@timer
def my_function():
    """
    функція створює список квадратів чисел від 0 до 999 999, а потім обчислює
    суму цих чисел
    """
    numbers = [x**2 for x in range(1000000)]
    return sum(numbers)


if __name__ == "__main__":
    my_function()
