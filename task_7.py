# Створіть звичайну функцію множення двох чисел. Створіть
# карированну функцію множення двох чисел. Частково застосуйте
# її до одного аргументу, до двох аргументiв.
def multiply(x, y):
    """Функція звичайного множення"""
    return x * y


def curried_multiply(x):
    """Застосування каррінгу до функції"""

    def inner(y):
        return x * y

    return inner


multiply_by_three = curried_multiply(3)
result1 = multiply_by_three(4)
print(result1)
print(curried_multiply(2)(3))
