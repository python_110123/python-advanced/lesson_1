# Напишіть програму яка буде виводити 25 перших чисел Фібоначі,
# використовуючи для цього три наведені в тексті заняття функції
# — без кешу, з кешем довільної довжини, з кешем з модулю functools
# з максимальною кількістю 10 елементів та з кешем з модулю functools
# з максимальною кількістю 16 елементів.


import functools


def get_fib_sequence_init_elements():
    """Повертає перші два елементи послідовності Фібоначчі (0 і 1)"""
    return [0, 1]


def generate_fibonacci_sequence(fib_sequence, num):
    """Генерує послідовність Фібоначчі до n-го елемента"""
    while len(fib_sequence) < num:
        fib_sequence.append(fib_sequence[-1] + fib_sequence[-2])
    return fib_sequence


def fibonacci(num):
    """Функція без кешування"""
    fib_sequence = get_fib_sequence_init_elements()
    return generate_fibonacci_sequence(fib_sequence, num)


@functools.lru_cache()
def fibonacci_with_cache(num):
    """Функція з кешуванням довільної довжини"""
    fib_sequence = get_fib_sequence_init_elements()
    return generate_fibonacci_sequence(fib_sequence, num)


@functools.lru_cache(maxsize=10)
def fibonacci_with_limited_cache(num):
    """
    Функція з кешуванням модулю functools з максимальною кількістю
    10 елементів
    """
    fib_sequence = get_fib_sequence_init_elements()
    return generate_fibonacci_sequence(fib_sequence, num)


@functools.lru_cache(maxsize=16)
def fibonacci_with_limited_cache_16(num):
    """
    Функція з кешуванням модулю functools з максимальною кількістю
    16 елементів
    """
    fib_sequence = get_fib_sequence_init_elements()
    return generate_fibonacci_sequence(fib_sequence, num)


if __name__ == "__main__":
    print("Перші 25 чисел Фібоначі без кешування:")
    print(fibonacci(25))

    print("Перші 25 чисел Фібоначі з кешем довільної довжини:")
    print(fibonacci_with_cache(25))

    print("Перші 25 чисел Фібоначі з кешем довжиною 10 елементів:")
    print(fibonacci_with_limited_cache(25))

    print("Перші 25 чисел Фібоначі з кешем довжиною 16 елементів:")
    print(fibonacci_with_limited_cache_16(25))
