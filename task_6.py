# Створіть функцію-генератор чисел Фібоначчі. Застосуйте
# до неї декоратор, який залишатиме в послідовності лише парні числа.
import functools


def even_filter(fibonacci_generator):
    """декоратор, який залишатиме в послідовності лише парні числа"""

    @functools.wraps(fibonacci_generator)
    def wrapper(*args, **kwargs):
        for num in fibonacci_generator(*args, **kwargs):
            if num % 2 == 0:
                yield num

    return wrapper


@even_filter
def fibonacci_generator(nums):
    """Функція-генератор чисел Фібоначчі"""
    x, y = 0, 1
    for _ in range(nums):
        x, y = y, x + y
        yield x


print(list(fibonacci_generator(18)))
